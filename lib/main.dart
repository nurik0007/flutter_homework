import 'package:characters/characters.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Kubiki(),
    );
  }
}

class Kubiki extends StatelessWidget {
  const Kubiki({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade400,
      appBar: AppBar(
        title: const Text('Меню',
            style: TextStyle(color: Color.fromARGB(255, 11, 11, 11))),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [
            Kubik(
              maimAxis: MainAxisAlignment.start,
            ),
            Kubik(
              maimAxis: MainAxisAlignment.center,
            ),
            Kubik(
              maimAxis: MainAxisAlignment.end,
            ),
          ],
        ),
      ),
    );
  }
}

class Kubik extends StatelessWidget {
  const Kubik({Key? key, required this.maimAxis}) : super(key: key);
  final MainAxisAlignment maimAxis;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: maimAxis,
      children: [
        Container(
          height: 50,
          width: 50,
          color: Colors.red,
          child: const Center(
            child: Text(
              '1',
              style: TextStyle(fontSize: 24),
            ),
          ),
        ),
        Container(
          height: 80,
          width: 80,
          color: Colors.yellow,
          child: const Center(
            child: Text(
              '2',
              style: TextStyle(fontSize: 24),
            ),
          ),
        ),
        Container(
          height: 120,
          width: 120,
          color: Colors.green,
          child: const Center(
            child: Text(
              '3',
              style: TextStyle(fontSize: 24),
            ),
          ),
        ),
      ],
    );
  }
}
